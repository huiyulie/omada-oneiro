// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
//#include "MyMissionItem.h"
#include "MyHUD.h"
#include "Interactable.h"
#include "FinalProjectCharacter.h"
#include "FinalProjectGameMode.generated.h"


UCLASS(minimalapi)
class AFinalProjectGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AFinalProjectGameMode(/*const class FObjectInitializer& ObjectInitializer*/);

	void Tick(float deltaTime) override;
	void BeginPlay();
	void SetHealthHUD();
	float GetHealthRemaining();
	void ResetHealth();
	void PauseHealth(); //pauses/unpauses
	void Blur();
	void StartDeathTimer();
	void DyingSound();
	void Death();
	void CheckMusicChange();
    AMyHUD* getHud() { return mHUD; }

	void checkInteract();
    void AddItem(class AInteractable* item);


private:

	float missionCompleted = 0.0f;

	TArray<class AInteractable*> interactableItems; // Used to track all items that have been interacted with. AMyMissionItem will be changed to AMyTakeableItem when the class is implemented. This class will hold the boolean to determine whether it has been interacted with.
    
	// When an item is interacted with, it should be added to the TArray.
	
    
	//Health timer consists of blurtimer + deathtimer
	//if mission is complete, then timer resets
	FTimerHandle blurTimer;
	FTimerHandle deathTimer;
	//amt of health player starts with (max seconds)
	UPROPERTY(EditAnywhere)
		float tilBlur = 300.0f; //til blur
	UPROPERTY(EditAnywhere)
		float tilDeath = 100.0f; //til death
	bool isDying = false;
	AMyHUD* mHUD;
	int songPlaying; //just to easily keep track of which song's playing!
    
    
};



