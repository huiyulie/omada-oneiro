// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"



UCLASS()
class FINALPROJECT_API AMyHUD : public AHUD
{
	GENERATED_BODY()
	
    UPROPERTY(EditAnywhere, Category=AMyHUD)
    UFont* Font;
    

public:
    AMyHUD(const FObjectInitializer& ObjectInitializer);
    virtual void DrawHUD() override;
	void Tick(float deltaTime) override;
    
    void DrawHealth(float Health);
    FString GetHealth() { return HealthString; }
    
    void DrawPickupMessage();
    void ErasePickupMessage();
	void Death();
	void Done();
    void DrawGeneralMessage(FString message);
    void HideGeneralMessage(FString message);
	bool fade;
    UFUNCTION(BlueprintCallable, Category="Backpack")
    FString GetBackpackString() { return BackpackString; }
    
    void OpenBackpack(TArray<FString> Backpack);
    void CloseBackpack();
    
    bool IsOpened() { return isOpened; }
    
    UFUNCTION(BlueprintCallable, Category="MainMenu")
    FString GetInstructions() { return Instructions; }

	FString GetGeneral() { return InteractableMessage; }

	void clearString();

private:
	void completeClear() { FinishString = ""; }

	FTimerHandle Timer;
    FString BackpackString;
    FString DeathString = "";
    FString HealthString;
    FString PickupMessage = "";
    FString InteractableMessage = "";
	FString FinishString = "";
    bool isOpened = false;
    FString Instructions = "Your head feels heavy and you feel slightly dizzy. Apparently, you have fallen into a nightmare, where time is constantly looping and the whole world is stuck in a loop. Explore the world and find out what is causing these endless loop. Fix them to get out of the nightmare before you lose consciousness and become a part of the nightmare too.\nCharacter Movement: Arrow Keys\nCamera Rotation: 'W' 'A' 'S' 'D' Keys\nJump: Space Bar\nInteract with objects: 'V' Key\nInventory: 'X' Key";
};
