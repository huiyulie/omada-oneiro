// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TakeableItem.h"
#include "Interactable.generated.h"

UCLASS()
class FINALPROJECT_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties

	AInteractable();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	FString getIName() { return name; }

	void setIName(FString n) { name = n; }

	FString getMatch() { return item; }

	void match( FString x);

    UFUNCTION(BlueprintCallable, Category="Dialogue")
	FString getDialogue();
	void setBefore(FString n) { before = n; }
	void setAfter(FString n) { after = n; }


	bool getStatus() { return finished; }

private:
	bool closeEnough();
	bool finished;
	float range;
	UPROPERTY(EditAnywhere)
	FString item;
	UPROPERTY(EditAnywhere)
	FString name;
	UPROPERTY(EditAnywhere)
	FString before;
	UPROPERTY(EditAnywhere)
	FString after;
    

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
	UStaticMesh* Meshes1;

	UPROPERTY(EditAnywhere)
	UStaticMesh* Meshes2;
	
};
