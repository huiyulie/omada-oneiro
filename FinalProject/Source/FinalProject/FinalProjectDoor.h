// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FinalProjectDoor.generated.h"


UCLASS()
class FINALPROJECT_API AFinalProjectDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFinalProjectDoor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    void OpenDoor();
    void CloseDoor();
    float range;
    bool IsOpen = false;
    
    UPROPERTY(EditAnywhere)
    class UStaticMeshComponent* Mesh;
	
};
