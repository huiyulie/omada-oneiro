// Fill out your copyright notice in the Description page of Project Settings.

#include "FinalProject.h"
#include "MyMissionItem.h"


// Sets default values

/*
 
 The following AMissionItem class is an abstract class used to represent the basic functionality of both items.
 We have two actual item types: interactable and takeable, which have varying functionality. 
 Takeable items should be able to be added to the bag.
 Interactable items don't need to be added to the bag, but
 */
//UCLASS(abstract)
AMyMissionItem::AMyMissionItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyMissionItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyMissionItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

