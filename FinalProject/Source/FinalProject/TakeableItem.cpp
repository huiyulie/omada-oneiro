// Fill out your copyright notice in the Description page of Project Settings.

#include "FinalProject.h"
#include "FinalProjectCharacter.h"
#include "TakeableItem.h"


// Sets default values
ATakeableItem::ATakeableItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    range = 250.0f;
    itemName = "";
	done = false;
    //HUDClass = AMyHUD::StaticClass();
    //mHud =
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));

}

// Called when the game starts or when spawned
void ATakeableItem::BeginPlay()
{
	Super::BeginPlay();
	((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->addRef(this);
}

bool ATakeableItem::CloseEnough()
{
    //auto player = UGameplayStatics::GetPlayerPawn(this, 0);
    if(UGameplayStatics::GetPlayerPawn(this, 0))
    {
        auto player = UGameplayStatics::GetPlayerPawn(this, 0);
        auto playerVector = player->GetActorLocation();
        float distance = FVector::Dist(GetActorLocation(), playerVector);
        if(distance - range > 0) // If not in range
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    return false; // not close enough
}

void ATakeableItem::setInvisible()
{
	done = true;
	Mesh->SetVisibility(false);
}

// Called every frame
void ATakeableItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    AMyHUD* HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	if (!done) {
		if (CloseEnough())
		{
			HUD->DrawPickupMessage();
			// Display HUD message saying "Press to pick up item"
		}
		else {
			HUD->ErasePickupMessage();
		}
	}
    
}


