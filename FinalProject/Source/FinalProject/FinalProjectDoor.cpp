// Fill out your copyright notice in the Description page of Project Settings.

#include "FinalProject.h"
#include "FinalProjectDoor.h"


// Sets default values
AFinalProjectDoor::AFinalProjectDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    range = 150.0f; // Setting the range of the door.
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
}

// Called when the game starts or when spawned
void AFinalProjectDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFinalProjectDoor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
    // Get the current players position using UGameplayStatics::GetPlayerPawn(this, 0)
    // Get the door's postion using GetPawn->GetActorLocation();
    // Calculate the distance between the two
    // If this distance is less than the range
        // OpenDoor();
    // Else
        // CloseDoor();

    auto doorVector = GetActorLocation();
    //auto doorVector = door->GetActorLocation();
    auto player = UGameplayStatics::GetPlayerPawn(this, 0);
    if(player)
    {
        auto playerVector = player->GetActorLocation();
        float distance = FVector::Dist(doorVector, playerVector);
        if(distance - range > 0) // If not in range and door is open
        {
            if(IsOpen)
            {
                CloseDoor();
            }
        }
        else // We're in range of the door.
        {
            if(!IsOpen)
            {
                OpenDoor();
            }
        }
    }
}

void AFinalProjectDoor::OpenDoor()
{
    // Rotate the door such that it is open. Rotate by an angle of pi/2.
	FRotator rotato = GetActorRotation();
	rotato.Yaw = rotato.Yaw + 90.0f;
    SetActorRotation(rotato);
    IsOpen = true;
}

void AFinalProjectDoor::CloseDoor()
{
    // Rotate the door such that it is closed. Rotate by an angle of pi/2.
    FRotator rotato = GetActorRotation();
	rotato.Yaw = rotato.Yaw - 90.0f;
    SetActorRotation(rotato);
    IsOpen = false;
}

