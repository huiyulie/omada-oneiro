// Fill out your copyright notice in the Description page of Project Settings.

#include "FinalProject.h"
#include "FinalProjectGameMode.h"
#include "FinalProjectCharacter.h"
#include "Interactable.h"


// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	PrimaryActorTick.bCanEverTick = true;
	range = 1000.0f;
	finished = false;
	name = "";
	item = "";
	before = "";
	after = "";
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
	
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	((AFinalProjectGameMode*)GetWorld()->GetAuthGameMode())->AddItem(this);
	Mesh->SetStaticMesh(Meshes1);
}

// Called every frame
void AInteractable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    AMyHUD* HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	if (closeEnough()) {
        auto player = (AFinalProjectCharacter*) UGameplayStatics::GetPlayerPawn(this, 0);
        if(finished) {
            HUD->DrawGeneralMessage(after);
        }
        else {
            HUD->DrawGeneralMessage(before);
        }
		//display HUD message
	}
	else {
		if (HUD->GetGeneral().Equals(before) || HUD->GetGeneral().Equals(after)) {
			HUD->DrawGeneralMessage("");
		}
		
	}

}


void AInteractable::match(FString x)
{
	if (x.Equals(item)) {
		if (finished == false && closeEnough() == true) {
			finished = true;
			Mesh->SetStaticMesh(Meshes2);
            AFinalProjectCharacter* player = ((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)));
            if(player)
            {
                player->PlaySuccess();
            }
            // increment completion status
		}
	}
	
}

FString AInteractable::getDialogue()
{
	if (finished) {
		return after;
	}
	else {
		return before;
	}
}


bool AInteractable::closeEnough()
{
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);
	if (player)
	{
		auto playerVector = player->GetActorLocation();
		float distance = FVector::Dist(GetActorLocation(), playerVector);
		if (distance - range > 0) // If not in range
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}

