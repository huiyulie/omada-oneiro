// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "TakeableItem.h"
#include "FinalProjectCharacter.generated.h"

UCLASS(config=Game)
class AFinalProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AFinalProjectCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	void setDeath(bool x) { death = x; }

	bool getDeath() { return death; }

	void Jump() override;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	//MUSIC
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* Song1;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* Song2;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* Song3;
	UPROPERTY(Transient)
		class UAudioComponent* Music;

	//SFX
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* deathSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* pickupSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* dyingSound;
	UPROPERTY(Transient)
		class UAudioComponent* sfx;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
    
    TArray<FString> backpack; // Unreal's version of a C++ vector 
    
	void Interact();
    
    void OpenBackpack();
    void CloseBackpack();

	bool death;

	void AddItem(ATakeableItem* item);

	TArray<ATakeableItem*> allRef;
    AMyHUD* HUD;

public:

	void addRef(ATakeableItem* i);
	FString FindItem(FString itemName);
	void RemoveItem(FString item);
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	void StopMusic();
	void PlaySong(/*class USoundCue* Song*/ int Song);
	UAudioComponent* PlaySFX(class USoundCue* Song);
	void PlayDeath();
	void PlayDying();
    void PlaySuccess();
};

