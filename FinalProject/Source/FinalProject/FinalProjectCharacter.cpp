// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "FinalProject.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "FinalProjectGameMode.h"
#include "FinalProjectCharacter.h"
#include "Sound/SoundCue.h"

//////////////////////////////////////////////////////////////////////////
// AFinalProjectCharacter

AFinalProjectCharacter::AFinalProjectCharacter()
{
	death = false;
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFinalProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFinalProjectCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
    
    // Project additions.
    PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AFinalProjectCharacter::Interact);
    
    // Open and close backpack items list
    PlayerInputComponent->BindAction("OpenBackpack", IE_Pressed, this, &AFinalProjectCharacter::OpenBackpack);
    PlayerInputComponent->BindAction("CloseBackpack", IE_Released, this, &AFinalProjectCharacter::CloseBackpack);
    
	PlayerInputComponent->BindAxis("MoveForward", this, &AFinalProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFinalProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFinalProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFinalProjectCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AFinalProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AFinalProjectCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFinalProjectCharacter::OnResetVR);
}


void AFinalProjectCharacter::Jump()
{
	if (!death) {
		Super::Jump();
	}

}

void AFinalProjectCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFinalProjectCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AFinalProjectCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AFinalProjectCharacter::TurnAtRate(float Rate)
{
	if (death != true) {
		// calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
	
}

void AFinalProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFinalProjectCharacter::MoveForward(float Value)
{
	if (death != true) {
		if ((Controller != NULL) && (Value != 0.0f))
		{
			// find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			AddMovementInput(Direction, Value);
		}
	}
	
}

void AFinalProjectCharacter::RemoveItem(FString item)
{
    for(auto temp : backpack)
    {
        if(temp.Equals(item))
        {
            backpack.Remove(temp);
        }
    }
}

void AFinalProjectCharacter::addRef(ATakeableItem * i)
{
	allRef.Add(i);
}

FString AFinalProjectCharacter::FindItem(FString itemName)
{
    for(auto item : backpack)
    {
        if(item.Equals(itemName))
        {
            return item;
        }
    }
    return ""; // Means item wasn't found
}

void AFinalProjectCharacter::Interact()
{
	if (death != true) {
		((AFinalProjectGameMode*)GetWorld()->GetAuthGameMode())->checkInteract();
		for (ATakeableItem* item : allRef) {
			if (item)
			{
				if (item->CloseEnough() == true) {
					auto tmp = item;
					PlaySuccess();
					//allRef.Remove(item);
					AddItem(tmp);
				}
			}
		}
	}
}

void AFinalProjectCharacter::PlaySuccess() {
    PlaySFX(pickupSound);
}

void AFinalProjectCharacter::AddItem(ATakeableItem * item)
{
	backpack.Add(item->GetItemName());
	item->setInvisible();
}

void AFinalProjectCharacter::MoveRight(float Value)
{
	if (death != true) {
		if ((Controller != NULL) && (Value != 0.0f))
		{
			// find out which way is right
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get right vector 
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
			// add movement in that direction
			AddMovementInput(Direction, Value);
		}
	}
}

void AFinalProjectCharacter::OpenBackpack() {
    HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
    if(HUD != nullptr) {
        HUD -> OpenBackpack(backpack);
    }
}

void AFinalProjectCharacter::CloseBackpack() {
    HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
    if(HUD != nullptr) {
        HUD -> CloseBackpack();
    }
}

//adapted from Lab 5
void AFinalProjectCharacter::PlaySong(/*class USoundCue* Song*/int Song) {
	if (Song==1) {
		Music = UGameplayStatics::SpawnSoundAttached(Song1, RootComponent);
	}else if (Song == 2) {
		Music = UGameplayStatics::SpawnSoundAttached(Song2, RootComponent);
	}else if (Song == 3) {
		Music = UGameplayStatics::SpawnSoundAttached(Song3, RootComponent);
	}
}
void AFinalProjectCharacter::StopMusic() {
	if (Music) {
		Music->Stop();
	}
}

UAudioComponent* AFinalProjectCharacter::PlaySFX(class USoundCue* Song) {
	UAudioComponent* AU = NULL;
	if (Song) {
		AU = UGameplayStatics::SpawnSoundAttached(Song, RootComponent);
	}
	return AU;
}

void AFinalProjectCharacter::PlayDeath() {
	if (sfx) {
		sfx->Stop();
	}
	sfx = UGameplayStatics::SpawnSoundAttached(deathSound, RootComponent);
}

void AFinalProjectCharacter::PlayDying() {
	if (sfx) {
		sfx->Stop();
	}
	sfx = UGameplayStatics::SpawnSoundAttached(dyingSound, RootComponent);
}
