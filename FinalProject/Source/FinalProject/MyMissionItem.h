

#pragma once

#include "GameFramework/Actor.h"
#include "MyMissionItem.generated.h"

UCLASS()
class FINALPROJECT_API AMyMissionItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyMissionItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
