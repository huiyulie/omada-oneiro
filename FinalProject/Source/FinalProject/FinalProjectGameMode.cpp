// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "FinalProject.h"
#include "FinalProjectGameMode.h"
#include "Kismet/GameplayStatics.h"



AFinalProjectGameMode::AFinalProjectGameMode()/*(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)*/
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
    HUDClass = AMyHUD::StaticClass();
	
}

void AFinalProjectGameMode::BeginPlay() {
	Super::BeginPlay();
    mHUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());

	//run AFTER constructors
	//mChar = new AFinalProjectCharacter();
	ResetHealth();
	//mHud = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(mChar, 0)->GetHUD());
	

	//play first song!
	((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->PlaySong(1);
	songPlaying = 1;

	if (UGameplayStatics::GetPlayerPawn(this, 0)) {
		for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.X = missionCompleted;
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Y = missionCompleted;
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Z = missionCompleted;
		}
	}
}

void AFinalProjectGameMode::checkInteract()
{
	for (AInteractable* item : interactableItems) {
		if (!(((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->FindItem(item->getMatch()).Equals("Default"))) {
			item->match(((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->FindItem(item->getMatch()));
		}
	}
}

void AFinalProjectGameMode::AddItem(class AInteractable* item)
{
    interactableItems.Add(item);
}

void AFinalProjectGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	int count = 0;
	for (AInteractable* item : interactableItems) {
		if (item->getStatus()) {
			count++;
		}
	}

       if ((float)((float)count / (float)interactableItems.Num()) > missionCompleted) {
            missionCompleted = (float)count / (float)interactableItems.Num();
           // ResetHealth();
			CheckMusicChange();
			//make camera bw
			if (UGameplayStatics::GetPlayerPawn(this, 0)) {
				for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
					((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.X = missionCompleted;
					((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Y = missionCompleted;
					((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Z = missionCompleted;
				}
			}
			if (missionCompleted == 1.0f) {
				GetWorldTimerManager().ClearTimer(deathTimer);
				GetWorldTimerManager().ClearTimer(blurTimer);
				for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
					((UCameraComponent*)comp)->PostProcessSettings.DepthOfFieldNearBlurSize = 0;
				}
				AMyHUD* HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
				HUD->Done();
			}
        }

	if (GetHealthRemaining() < tilDeath && GetHealthRemaining() > 0) {
		Blur();
	}
	else {
		if (UGameplayStatics::GetPlayerPawn(this, 0)) {
			for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
				((UCameraComponent*)comp)->PostProcessSettings.DepthOfFieldNearBlurSize = 0.0f;
			}
		}
	}

	SetHealthHUD(); //update HUD
	
}


// HEALTH TIMER SHENANIGANS /////////////////////////

//called upon blurtimer done!
void AFinalProjectGameMode::StartDeathTimer() {
	//start death timer
	isDying = true;
	GetWorldTimerManager().SetTimer(deathTimer, this, &AFinalProjectGameMode::Death, tilDeath, false);
}

//called every tick after blur timer is done until death
void AFinalProjectGameMode::Blur() {
	//trigger camera blurring
	if (UGameplayStatics::GetPlayerPawn(this, 0)) {
		for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
			((UCameraComponent*)comp)->PostProcessSettings.DepthOfFieldNearBlurSize = ((float)GetHealthRemaining()/tilDeath) * 20.0f;
		}
		FTimerHandle dying;
		GetWorldTimerManager().SetTimer(dying, this, &AFinalProjectGameMode::DyingSound, 1, false);
	}
}

void AFinalProjectGameMode::DyingSound() {
	((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->PlayDying();
}

//called upon deathtimer done!
void AFinalProjectGameMode::Death() {
	//trigger state of death
	//update UI
	AMyHUD* HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	HUD->Death();
	if (UGameplayStatics::GetPlayerPawn(this, 0)) {
		((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)))->PlayDeath();
	}

}

void AFinalProjectGameMode::ResetHealth() {
	GetWorldTimerManager().SetTimer(blurTimer, this, &AFinalProjectGameMode::StartDeathTimer, tilBlur, false);
	GetWorldTimerManager().ClearTimer(deathTimer);
	isDying = false;
}

void AFinalProjectGameMode::PauseHealth() {
	if (!isDying) { //hasn't started death timer yet
		if(GetWorldTimerManager().IsTimerActive(blurTimer)){
			GetWorldTimerManager().PauseTimer(blurTimer);
		}
		else {
			GetWorldTimerManager().UnPauseTimer(blurTimer);
		}
	}
	else { //ur dying bro
		if (GetWorldTimerManager().IsTimerActive(deathTimer)) {
			GetWorldTimerManager().PauseTimer(deathTimer);
		}
		else {
			GetWorldTimerManager().UnPauseTimer(deathTimer);
		}
	}
}

float AFinalProjectGameMode::GetHealthRemaining() {
	if (!isDying) { //hasn't started death timer yet
		return GetWorldTimerManager().GetTimerRemaining(blurTimer) + tilDeath;
	}
	else {
		return GetWorldTimerManager().GetTimerRemaining(deathTimer);
	}
}

void AFinalProjectGameMode::SetHealthHUD() {
	AMyHUD* HUD = Cast<AMyHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	float health = GetHealthRemaining();
	if (health >= 0) {
		HUD->DrawHealth(GetHealthRemaining());
	}
}



void AFinalProjectGameMode::CheckMusicChange() {
	AFinalProjectCharacter* player = ((AFinalProjectCharacter*)(UGameplayStatics::GetPlayerPawn(this, 0)));
	
	if (player) {
		if (missionCompleted > 0.8 && songPlaying != 3) {
			player->StopMusic();
			player->PlaySong(3);
			songPlaying = 3;
		}
		else if (missionCompleted > 0.4 && songPlaying != 2) {
			player->StopMusic();
			player->PlaySong(2);
			songPlaying = 2;
		}
		else if (songPlaying != 1) {
			player->StopMusic();
			player->PlaySong(1);
			songPlaying = 1;
		}
	}
}