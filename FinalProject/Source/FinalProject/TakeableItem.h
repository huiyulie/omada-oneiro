// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MyHUD.h"
#include "TakeableItem.generated.h"

UCLASS()
class FINALPROJECT_API ATakeableItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATakeableItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    void SetItemName(FString name) { itemName = name; }
    FString GetItemName() { return itemName; }
    bool CloseEnough();
	void setInvisible();


	
private:
    float range;
	bool done;

	UPROPERTY(EditAnywhere)
    FString itemName;
    
    UPROPERTY(EditAnywhere)
    class UStaticMeshComponent* Mesh;


};
