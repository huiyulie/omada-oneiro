// Fill out your copyright notice in the Description page of Project Settings.

#include "FinalProject.h"
#include "MyHUD.h"
#include "FinalProjectCharacter.h"

AMyHUD::AMyHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {
	fade = false;
}

void AMyHUD::Tick(float deltaTime) {
	Super::Tick(deltaTime);
	if (fade == true) {
		if (UGameplayStatics::GetPlayerPawn(this, 0)) {
			for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
				((UCameraComponent*)comp)->PostProcessSettings.DepthOfFieldNearBlurSize += 1.0f;
			}
		}
	}
}

void AMyHUD::DrawHUD() {
    Super::DrawHUD();
    
    FString OpenBagString = "Press X to open bag";

    DrawText(OpenBagString, FColor::White, 10, 10, NULL, 2);

    DrawText(HealthString, FColor::White, 10, 40, NULL, 2);
    
	DrawText(DeathString, FColor::Red, 500, 300, NULL, 5.0f);
    
    DrawText(PickupMessage, FColor::White, 10, 80, NULL, 2);
    
    DrawText(InteractableMessage, FColor::White, 10, 200, NULL, 2);

	DrawText(FinishString, FColor::Green, 500, 300, NULL, 5.0f);
}

void AMyHUD::DrawHealth(float Health) {
    HealthString = "Health: ";
	HealthString.AppendInt((int)(Health+0.5));
}

void AMyHUD::OpenBackpack(TArray<FString> Backpack) {
    BackpackString = "Backpack:\n";
    for(auto item : Backpack) {
        BackpackString.Append(item + "\n");
    }
}

void AMyHUD::CloseBackpack() {
    
}

void AMyHUD::clearString()
{
	if (!DeathString.IsEmpty()) {
		DeathString = "Press Z for Menu";
	}
	else if (!FinishString.IsEmpty()) {
		FinishString = " Press P to Go Back to Menu or Explore the World for Fun!";
		GetWorldTimerManager().SetTimer(Timer, this, &AMyHUD::completeClear, 3.0f, false);
	}
	else {
		DeathString = "";
		FinishString = "";
	}
}

void AMyHUD::DrawPickupMessage() {
    PickupMessage = "Press V to pick up the item";
}

void AMyHUD::ErasePickupMessage() {
    PickupMessage = "";
}

void AMyHUD::Death() {
	DeathString = "YOU DIED!";
	if (UGameplayStatics::GetPlayerPawn(this, 0)) {
		for (UActorComponent* comp : UGameplayStatics::GetPlayerPawn(this, 0)->GetComponentsByClass(UCameraComponent::StaticClass())) {
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.X = 0.0f;
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Y = 0.0f;
			((UCameraComponent*)comp)->PostProcessSettings.ColorSaturation.Z = 0.0f;
		}
		((AFinalProjectCharacter*)UGameplayStatics::GetPlayerPawn(this, 0))->setDeath(true);
	}
	fade = true;
	GetWorldTimerManager().SetTimer(Timer, this, &AMyHUD::clearString, 3.0f, false);
}

void AMyHUD::Done()
{
	FinishString = "Cleared!";
	GetWorldTimerManager().SetTimer(Timer, this, &AMyHUD::clearString, 3.0f, false);
}

void AMyHUD::DrawGeneralMessage(FString message) {
    InteractableMessage = message;
}

void AMyHUD::HideGeneralMessage(FString message) {
    InteractableMessage = "";
}

